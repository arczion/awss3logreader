﻿using Amazon.S3;
using Amazon.S3.Model;
using BusinessLayer;
using BusinessLayer.AWS;
using System;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;

namespace ElasticCommunicator
{
    class Program
    {
        /// <summary>
        /// Enrty point of the program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //Create S3 client object
            AWSS3ClientHelper s3Helper = new AWSS3ClientHelper();
            var _s3Client = s3Helper.GetS3Client();

            ProcessS3AccessLogs(_s3Client).Wait();
        }



        /// <summary>
        /// AWS S3 Bukcet acess logs
        /// </summary>
        /// <param name="_s3Client"></param>
        /// <returns></returns>
        private static async Task ProcessS3AccessLogs(IAmazonS3 _s3Client)
        {

            IOHelper ioHelper = new IOHelper();

            DataTable s3AccessLogDatatable = null;
            bool result = false;
            AWSHelper awsHelper = new AWSHelper();
            string logFilePath = string.Empty;

            try
            {
                ListObjectsRequest request = new ListObjectsRequest
                {
                    BucketName = ConfigurationManager.AppSettings["RootBucketName"],
                    Prefix = ConfigurationManager.AppSettings["S3BucketPath"],
                    MaxKeys = 10
                };
                ListObjectsResponse response;
                do
                {

                    response = await _s3Client.ListObjectsAsync(request);
                    // If response is truncated, set the marker to get the next 
                    // set of keys.
                    if (response.IsTruncated)
                    {
                        request.Marker = response.NextMarker;
                    }
                    else
                    {
                        request = null;
                    }

                    if (response.IsTruncated)
                    {
                        //Process the response.
                        foreach (S3Object entry in response.S3Objects)
                        {
                            //Read the data from log file
                            s3AccessLogDatatable = awsHelper.ReadS3AccessLogs(entry, _s3Client, out logFilePath);

                            if (s3AccessLogDatatable != null && s3AccessLogDatatable.Rows.Count > 0)
                            {


                                //Once data is onboarded delete the log file
                                if (result)
                                {
                                    //Delete the file
                                    ioHelper.DeleteFile(logFilePath);
                                }
                            }
                        }
                    }


                } while (response.IsTruncated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}

