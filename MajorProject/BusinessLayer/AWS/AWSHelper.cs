﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Amazon.S3.IO;
using System.Reflection;
using System.Data;
using System.Configuration;
using System.Collections.Specialized;

namespace BusinessLayer.AWS
{
    public class AWSHelper
    {
        /// <summary>
        ///     AWS Get an object
        /// </summary>
        /// <param name="bucketName"></param>
        /// <param name="keyName">Friendly Name</param>
        public bool GetObject(IAmazonS3 _s3Client, string keyName, string tempPath, out string downloadPath)
        {
            bool status = false;
            string fileName = GetSafeFilename(keyName);

            //Add .log as file extension for the S3 access log files
            fileName = keyName + ".log";
            try
            {


                GetObjectRequest request = new GetObjectRequest
                {
                    BucketName = ConfigurationManager.AppSettings["RootBucketName"],
                    Key = keyName
                };


                using (GetObjectResponse response = _s3Client.GetObject(request))
                {

                    string dest = Path.Combine(tempPath, fileName);
                    if (!File.Exists(dest))
                    {
                        response.WriteResponseStreamToFile(dest);
                        status = true;
                    }
                }

            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                // Catch AWS exception.
                if (amazonS3Exception.ErrorCode != null && (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId") || amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
                {

                }
                else
                {

                }
                status = false;
            }


            downloadPath = fileName;

            return status;
        }

        /// <summary>
        /// Read logs from the folder
        /// </summary>
        /// <param name="s3object"></param>
        /// <param name="_s3Client"></param>
        /// <param name="logFilePath"></param>
        /// <returns></returns>
        public DataTable ReadS3AccessLogs(S3Object s3object, IAmazonS3 _s3Client, out string logFilePath)
        {

            bool isFileDownloaded = false;
            string unzipFilePath = string.Empty;

            IOHelper ioHelper = new IOHelper();


            string downloadPath = string.Empty;

            string awsLogPath = ioHelper.GetLogFilePath(ConfigurationManager.AppSettings.Get("LocalFolderPath"));

            string tempPath = awsLogPath != null || string.IsNullOrEmpty(awsLogPath) ? awsLogPath : null;
            logFilePath = string.Empty;

            isFileDownloaded = GetObject(_s3Client, s3object.Key, tempPath, out downloadPath);

            if (isFileDownloaded)
            {

                //The log file is unzipped succesfully, read the log file and create data table for onboarding
                //s3AccessLogDatatable = S3DataTableHelper.GetDatatableForS3AccessLogFile(tempPath + "/" + downloadPath);
                //logFilePath = tempPath + "/" + downloadPath;

            }


            return null;

        }

        private string GetSafeFilename(string fileName)
        {
            string safeFileName = null;
            string path = null;

            if (fileName != null && !string.IsNullOrEmpty(fileName))
            {
                try
                {
                    safeFileName = fileName.Substring(fileName.LastIndexOf('/') + 1);
                    safeFileName = string.Join("_", safeFileName.Split(Path.GetInvalidFileNameChars()));
                    path = fileName.Substring(0, fileName.LastIndexOf('/') + 1) + safeFileName;

                }
                catch (Exception ex)
                {

                }
            }
            else
            {

            }


            return path;
            //path + safeFileName;

        }
    }
}
