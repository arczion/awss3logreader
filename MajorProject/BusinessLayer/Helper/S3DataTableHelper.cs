﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace BusinessLayer
{
    public class S3DataTableHelper
    {
        public static DataTable GetDatatableForS3AccessLogFile(string logFilePath)
        {
            String line;
            DataTable S3AccessLogDataTable = S3AccessDataTable();
            try
            {
                using (FileStream fileStream = new FileStream(logFilePath, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader streamReader = new StreamReader(fileStream))
                    {
                        //Read the contents of file line by line
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            while (!streamReader.EndOfStream)
                            {
                                line = streamReader.ReadLine();

                                DataRow dr = S3AccessDataTable().NewRow();

                                try
                                {
                                    if (line != null || !string.IsNullOrEmpty(line))
                                    {
                                        var grokObject = GROKHelper.GetGROKResponseForS3(line);

                                        Dictionary<string, object> dict = grokObject.Captures.ToDictionary(x => x.Item1, x => x.Item2);

                                        S3AccessLogDataTable.Rows.Add(S3AccessDataRow(dict, dr).ItemArray);
                                    }

                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

            return S3AccessLogDataTable;

        }

        private static DataTable S3AccessDataTable()
        {
            DataTable dt = new DataTable();
            dt.TableName = "S3AccessLogs";

            //Unique key column with GUID
            dt.Columns.Add("guid", typeof(String));
            dt.Columns.Add("time", typeof(DateTime));
            dt.Columns.Add("bucketowner", typeof(String));
            dt.Columns.Add("bucket", typeof(String));
            dt.Columns.Add("remoteip", typeof(String));
            dt.Columns.Add("requester", typeof(String));
            dt.Columns.Add("requestid", typeof(String));
            dt.Columns.Add("operation", typeof(String));
            dt.Columns.Add("key", typeof(String));
            dt.Columns.Add("requesttype", typeof(String));
            dt.Columns.Add("requesturi", typeof(String));
            dt.Columns.Add("version", typeof(Decimal));
            dt.Columns.Add("httpstatus", typeof(Int64));
            dt.Columns.Add("errorcode", typeof(String));
            dt.Columns.Add("bytesent", typeof(Int64));
            dt.Columns.Add("objectsize", typeof(Int64));
            dt.Columns.Add("totaltime", typeof(Int64));
            dt.Columns.Add("turnaroundtime", typeof(Int64));
            dt.Columns.Add("referrer", typeof(String));
            dt.Columns.Add("useragent", typeof(String));
            dt.Columns.Add("versionid", typeof(String));


            return dt;
        }

        private static DataRow S3AccessDataRow(Dictionary<string, object> dict, DataRow dr)
        {
            try
            {
                object tempValue = null;

                foreach (var key in dict.Keys)
                {
                    // Add the GUID in the column for primary key
                    dr["guid"] = Guid.NewGuid();
                    if (dict.TryGetValue(key, out tempValue))
                    {
                        #region<Time>
                        if (key == "time")
                        {
                            try
                            {

                                var dto = DateTimeOffset.ParseExact(tempValue.ToString(), "dd/MMM/yyyy:HH:mm:ss zzzz", null);
                                var utcTime = dto.UtcDateTime;

                                dr[key] = tempValue == null ? (object)DBNull.Value : utcTime;

                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        #endregion

                        else if (key == "bytesent" || key == "objectsize" || key == "turnaroundtime")
                        {
                            dr[key] = (tempValue == null || tempValue.ToString().Equals("-")) ? 0 : tempValue;

                        }
                        else
                        {
                            dr[key] = tempValue == null ? (object)DBNull.Value : tempValue;
                        }

                    }

                }
            }
            catch (Exception ex)
            {

            }

            return dr;
        }
    }
}
