﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class IOHelper
    {
        /// <summary>
        /// Deletes the file from system
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        public bool DeleteFile(string filePath)
        {
            bool status = false;
            try
            {
                if (!string.IsNullOrEmpty(filePath.Trim()) && filePath != null)
                {
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                        status = true;
                    }
                    else
                    {
                        status = false;
                    }
                }

            }
            catch (Exception ex)
            {

            }


            return status;
        }
        /// <summary>
        /// Get's the temporary path for storing the log file
        /// </summary>
        /// <param name="tempLogFolder"></param>
        /// <returns></returns>
        public string GetLogFilePath(string tempLogFolder)
        {

            string path = string.Empty;

            try
            {
                if (tempLogFolder.EndsWith("/"))
                {
                    path = tempLogFolder.TrimStart('/');
                }
                else
                {
                    path = tempLogFolder;
                }
            }
            catch (Exception ex)
            {

            }
            return path;
        }

        /// <summary>
        /// Read file content as String
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetFileContentAsString(string path)
        {
            string data = null;

            if (path != null && !string.IsNullOrEmpty(path))
            {
                try
                {
                    using (StreamReader stream = new StreamReader(path))
                    {
                        data = stream.ReadToEnd();
                    }
                }
                catch (Exception ex)
                {

                }
            }
            else
            {

            }


            return data;

        }
    }
}
