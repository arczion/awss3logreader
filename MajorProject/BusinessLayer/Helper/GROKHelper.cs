﻿using GrokDotNet;
using System;

namespace BusinessLayer
{
    public class GROKHelper
    {

        public static GrokParseResponse GetGROKResponseForS3(string S3AccessLogLine)
        {
            GROKConstant grokConstant = new GROKConstant();

            var s3AccessLogsGROK = new Grok(grokConstant.S3LOGGROKEXPRESSION);
            var response = s3AccessLogsGROK.ParseLine("");

            try
            {
                if (S3AccessLogLine != null || !string.IsNullOrEmpty(S3AccessLogLine))
                {
                    response = s3AccessLogsGROK.ParseLine(S3AccessLogLine);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return response;
        }

    }
}
