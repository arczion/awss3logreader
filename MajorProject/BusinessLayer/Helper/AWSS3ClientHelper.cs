﻿using Amazon;
using Amazon.S3;
using System.Configuration;

namespace BusinessLayer
{
    public class AWSS3ClientHelper
    {
        private readonly RegionEndpoint bucketRegion = RegionEndpoint.APSoutheast2;

        /// <summary>
        /// Get’s the S3 client
        /// </summary>
        /// <returns></returns>
        public IAmazonS3 GetS3Client()
        {
            IAmazonS3 S3Client = new AmazonS3Client(ConfigurationManager.AppSettings["AWSPrivateKey"], ConfigurationManager.AppSettings["AWSSecretKey"], bucketRegion);
            return S3Client;
        }
    }
}
