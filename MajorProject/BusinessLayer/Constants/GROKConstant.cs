﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class GROKConstant
    {

        private const string s3loggrokexpression = "%{NOTSPACE:bucketowner} %{NOTSPACE:bucket} \\[%{HTTPDATE:time}\\] %{IP:remoteip} %{NOTSPACE:requester} %{NOTSPACE:requestid} %{NOTSPACE:Operation} %{NOTSPACE:Key} (?:\"%{WORD:requesttype} %{URIPATHPARAM:requesturi} HTTP/%{NUMBER:version}\" %{NUMBER:httpstatus} %{NOTSPACE:errorcode} %{NOTSPACE:bytesent} %{NOTSPACE:objectsize} %{NUMBER:totaltime} %{NOTSPACE:turnaroundtime} %{NOTSPACE:referrer} %{GREEDYDATA:useragent} %{NOTSPACE:versionid})";

        public string S3LOGGROKEXPRESSION
        {
            get
            {
                return s3loggrokexpression;
            }
        }

       
    }

}
